FROM fedora:38 as builder

RUN dnf install -y cmake gcc-c++ make libxml2-devel physfs-devel sqlite-devel \
    lua-devel libsigc++20-devel rapidjson-devel

ADD . /source

WORKDIR /source/libs/uWebSockets
RUN make default

WORKDIR /source
RUN cmake . -DCMAKE_INSTALL_PREFIX=/app
RUN make -j$(nproc) install

FROM fedora:38
RUN dnf update -y && \
    dnf install -y libxml2 physfs sqlite lua libsigc++20 && \
    dnf clean all -q && \
    rm -rf /var/cache/dnf/*

COPY --from=builder /app/ /app/
